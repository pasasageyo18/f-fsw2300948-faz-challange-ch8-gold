const players = [
  {
    id: "96174447-f792-462f-ab37-14cd99730f11",
    username: "jaddah",
    email: "jaddah@gmail.com",
    experience: "800",
    lvl: "10",
  },
  {
    id: "96174447-f792-462f-ab37-14cd99730f12",
    username: "budi",
    email: "budi@gmail.com",
    experience: "0",
    lvl: "1",
  },
  {
    id: "96174447-f792-462f-ab37-14cd99730f13",
    username: "gogoz",
    email: "gogoz@gmail.com",
    experience: "0",
    lvl: "1",
  },
];

export default players;
