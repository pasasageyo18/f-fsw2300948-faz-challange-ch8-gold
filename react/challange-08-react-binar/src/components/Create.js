import { useState } from "react";
import { v4 as uuid } from "uuid";
import { Form, FormGroup, Input, Label, Button } from "reactstrap";
import { useNavigate } from "react-router-dom";
import players from "./array";

//Function to create a player
function Create() {
  //define the state
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [exp, setExp] = useState(0);
  const [lvl, setLvl] = useState(0);

  // //define the navigate
  let history = useNavigate();

  //to handle submit
  const handleSubmit = (e) => {
    e.preventDefault();

    const id = uuid(); //id set base on uuid function

    //create a new player object
    const newPlayer = {
      id: id,
      username: username,
      email: email,
      experience: exp,
      lvl: lvl,
    };

    players.push(newPlayer); //push the new player into array

    history("/");
  };

  return (
    <div className="d-flex justify-content-center p-3">
      <Form onSubmit={handleSubmit} className="col-6">
        <FormGroup>
          <Label for="username">Username</Label>
          <Input
            id="username"
            name="username"
            placeholder="Input username"
            type="text"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input
            id="email"
            name="email"
            placeholder="Input email"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="experience">Experience</Label>
          <Input
            id="experience"
            name="experience"
            placeholder="Input experience"
            type="number"
            value={exp}
            onChange={(e) => setExp(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="lvl">Level</Label>
          <Input
            id="lvl"
            name="lvl"
            placeholder="Input lvl"
            type="number"
            value={lvl}
            onChange={(e) => setLvl(e.target.value)}
          />
        </FormGroup>
        <Button type="submit" color="primary">
          Submit
        </Button>
      </Form>
    </div>
  );
}

export default Create;
