import { useEffect, useState } from "react";
import { Form, FormGroup, Input, Label, Button } from "reactstrap";
import { useNavigate, Link } from "react-router-dom";
import players from "./array";

//Function to edit player
function Edit() {
  //define state
  const [id, setId] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [exp, setExp] = useState(0);
  const [lvl, setLvl] = useState(0);

  //define navigate
  let history = useNavigate();

  //take players id
  let index = players
    .map(function (e) {
      return e.id;
    })
    .indexOf(id);

  //to handle submit
  const handleSubmit = (e) => {
    e.preventDefault();

    let player = players[index]; //get player base on the index

    //set the new player's data
    player.username = username;
    player.email = email;
    player.experience = exp;
    player.lvl = lvl;

    history("/"); //redirect to home
  };

  //to take the data from localStorage
  useEffect(() => {
    const storedId = localStorage.getItem("id");
    const storedUsername = localStorage.getItem("username");
    const storedEmail = localStorage.getItem("email");
    const storedExp = localStorage.getItem("experience");
    const storedLvl = localStorage.getItem("lvl");

    //set the input value base on the stored data
    setId(storedId || ""); // empty string as a default value if data is not found
    setUsername(storedUsername || "");
    setEmail(storedEmail || "");
    setExp(storedExp || 0);
    setLvl(storedLvl || 0);
  }, []);

  return (
    <div className="d-flex justify-content-center p-3">
      <Form className="col-6">
        <FormGroup>
          <Label for="username">Username</Label>
          <Input
            id="username"
            name="username"
            placeholder="Input username"
            type="text"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="email">Email</Label>
          <Input
            id="email"
            name="email"
            placeholder="Input email"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="experience">Experience</Label>
          <Input
            id="experience"
            name="experience"
            placeholder="Input experience"
            type="number"
            value={exp}
            onChange={(e) => setExp(e.target.value)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="lvl">Level</Label>
          <Input
            id="lvl"
            name="lvl"
            placeholder="Input lvl"
            type="number"
            value={lvl}
            onChange={(e) => setLvl(e.target.value)}
          />
        </FormGroup>
        <Button
          onClick={(e) => handleSubmit(e)}
          className="col-12 mb-2"
          color="success"
        >
          Update
        </Button>

        <Link to="/">
          <Button color="warning" className="col-12">
            Home
          </Button>
        </Link>
      </Form>
    </div>
  );
}

export default Edit;
