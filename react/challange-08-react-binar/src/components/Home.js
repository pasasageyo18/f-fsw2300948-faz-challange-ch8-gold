import { useNavigate, Link } from "react-router-dom";
import players from "./array";
import { Button, Table } from "reactstrap";
import { useState } from "react";

function Home() {
  //define the state
  const [sortBy, setSortBy] = useState("");
  const [searchQuery, setSearchQuery] = useState("");

  //filter player function to use it for search
  const filteredPlayers = players.filter((player) => {
    const lowerCaseQuery = searchQuery.toLowerCase();
    return (
      player.username.toLowerCase().includes(lowerCaseQuery) ||
      player.email.toLowerCase().includes(lowerCaseQuery) ||
      player.experience.includes(searchQuery) ||
      player.lvl.includes(searchQuery)
    );
  });

  // empty array for starter
  let sortedPlayers = [];

  //conditions for sorting the array
  if (sortBy === "") sortedPlayers = filteredPlayers;

  if (sortBy === "username")
    sortedPlayers = filteredPlayers
      .slice()
      .sort((a, b) => a.username.localeCompare(b.username));

  if (sortBy === "email")
    sortedPlayers = filteredPlayers
      .slice()
      .sort((a, b) => a.email.localeCompare(b.email));

  if (sortBy === "exp")
    sortedPlayers = filteredPlayers
      .slice()
      .sort((a, b) => parseInt(a.experience) - parseInt(b.experience));

  if (sortBy === "lvl")
    sortedPlayers = filteredPlayers
      .slice()
      .sort((a, b) => parseInt(a.experience) - parseInt(b.experience));

  let history = useNavigate();

  // store the object data to the local storage and pass it for editing
  function setPlayer(id, username, email, exp, lvl) {
    localStorage.setItem("id", id);
    localStorage.setItem("username", username);
    localStorage.setItem("email", email);
    localStorage.setItem("experience", exp);
    localStorage.setItem("lvl", lvl);
  }

  //delete data function
  function deleted(id) {
    let index = players
      .map(function (e) {
        return e.id;
      })
      .indexOf(id);

    players.splice(index, 1); //delete method base on index

    history("/");
  }

  return (
    <div className="p-3">
      <div className="navbar navbar-light bg-light navbar-expand-lg">
        <div className="container-fluid px-4">
          <p className="navbar-brand align-self-center">Welcome</p>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse justify-content-end">
            <ul className="navbar-nav">
              <li className="nav-item">
                <input
                  className="form-control me-2"
                  type="text"
                  placeholder="Search"
                  value={searchQuery}
                  onChange={(e) => setSearchQuery(e.target.value)}
                />
              </li>
              <li className="nav-item ms-3">
                <select
                  className="btn btn-primary"
                  onChange={(e) => setSortBy(e.target.value)}
                >
                  <option value="">Sort</option>
                  <option value="username">Username</option>
                  <option value="email">Email</option>
                  <option value="exp">Experience</option>
                  <option value="lvl">Level</option>
                </select>
              </li>
              <li className="nav-item ms-3">
                <Link to="/create">
                  <Button color="success">Create</Button>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <Table striped>
        <thead>
          <tr>
            <th>Username</th>
            <th>Email</th>
            <th>Experience</th>
            <th>Level</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {sortedPlayers.map((player) => {
            return (
              <tr key={player.id}>
                <td>{player.username}</td>
                <td>{player.email}</td>
                <td>{player.experience}</td>
                <td>{player.lvl}</td>
                <td>
                  <Link to="/edit">
                    <Button
                      onClick={(e) =>
                        setPlayer(
                          player.id,
                          player.username,
                          player.email,
                          player.experience,
                          player.lvl
                        )
                      }
                      color="info"
                    >
                      Edit Player
                    </Button>
                  </Link>
                </td>
                <td>
                  <Button onClick={(e) => deleted(player.id)} color="danger">
                    Delete Player
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
}

export default Home;
